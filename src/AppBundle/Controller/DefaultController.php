<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig'); 
    }
    
    /**
     * @Route("/home", name="Memki")
     */
    public function homeAction() {
        $em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM uploads WHERE votes >'5'" );
        $statement->execute();
        $results = $statement->fetchAll();
        return $this->render('default/home.html.twig', ['memy' => $results, 'images_dir' => '/pictures/upload']);
    }

    /**
     * @Route("/queue", name="Kolejeczka")
     */
    public function queueAction(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $queue = $entityManager->getRepository(\AppBundle\Entity\Uploads::class)->findBy([], ['updatedAt' => 'DESC']);


        return $this->render('default/queue.html.twig', ['Kolejeczka' => $queue, 'images_dir' => '/pictures/upload']);
        
        
    }
    
    /**
     * @Route("/queue/mem/{id}", name="mem")
     */
    
    public function memAction($id, Request $request) {
       
       $entityManager = $this->getDoctrine()->getManager();
       $mem = $entityManager->getRepository(\AppBundle\Entity\Uploads::class)->findOneBy(['id' => $id]);
       $voted = false;
       
       if ($request->getMethod() === 'POST') {
           $actual = $mem->getVotes();
           $mem->setVotes($actual + 1);
           $entityManager->persist($mem);
           $entityManager->flush();
           $voted = true;
       }
       
       return $this->render('default/mem.html.twig', ['mem' => $mem, 'images_dir' => '/pictures/upload', 'voted' => $voted]);
   }
    

    /**
     * @Route("/streams", name="Strumyczki")
     */
    public function streamsAction(Request $request)
    {
        return $this->render('default/streams.html.twig');
    }
    
    
    /**
     * @Route("/add", name="Dodaj")
     */
    public function addAction(Request $request)
    {
        if ($request->files->get('imageFile')) {
           
           $up = new \AppBundle\Entity\Uploads();
           
           $up->setImageFile($request->files->get('imageFile'));
           $up->setImageName($request->get('imageName'));
           $up->setImageSize(filesize($request->files->get('imageFile')));
           
           $em = $this->getDoctrine()->getManager();
           $em->persist($up);
           $em->flush();
           
       }
       
       
       return $this->render('default/add.html.twig');
    }
        
    
    
//    /**
//     * @Route("/login", name="Zaloguj")
//     */
//    public function signinAction(Request $request)
//    {
//        return $this->render('default/signin.html.twig');
//    }
    
    
    
//    /**
//     * @Route("/register", name="Nie masz konta?")
//     */
//    public function joinusAction(Request $request)
//    {    
//        return $this->render('default/joinus.html.twig');
//    }
    

}
